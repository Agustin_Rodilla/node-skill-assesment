const https = require('https');

exports.syncUsers = (userRepository) => {
  return async (req, res) => {
    this.callerGitUsers(userRepository, res);
  }
}

exports.callerGitUsers = async (userRepository, res) => {
  const options = {
    hostname: 'api.github.com',
    path: '/search/users?q=payflow',
    headers: { 'User-Agent': 'Mozilla/5.0' }
  };
  await https.get(options, (resF) => {
    resF.setEncoding('utf8');
    const responserPile = [];
    resF.on('data', (datas) => {
      responserPile.push(datas);
    });
    resF.on('end', async () => {
      const resultProcess = await this.managerResponseGitUsers(userRepository, JSON.parse(responserPile.join('')));
      if (resultProcess.length === 0) {
        res.sendStatus(200);
      } else {
        res.status(501).send(resultProcess);
      }
    });
  });
}

exports.managerResponseGitUsers = async (userRepository, objUsers) => {
    const bodyWork = userRepository.findAllUsers();
    const errorsInProcesate = [];
    if (!objUsers.items) {
      return [{
        error: 'invalitFormat, have no items'
      }];
    }
    for(const userNode of objUsers.items) {
      const workNode = adapterSourceUserFormat(userNode);
      const indexLocation = seekerUser(bodyWork, workNode);
      try {
        await executeAddOrUodate(userRepository, workNode, indexLocation)
      } catch (e) {
        errorsInProcesate.push({
          error: e.message,
          nodeOrigin: userNode
        });
        bodyWork.splice(indexLocation, 1);
      }
    }
    cleanExtraUsers(userRepository, bodyWork);
    return errorsInProcesate;
}

const executeAddOrUodate = async (userRepository, workNode, indexLocation) => {
  if (indexLocation === -1) {
    await userRepository.addUser(workNode);
  } else {
    userRepository.updateUser(workNode);
  }
}

const cleanExtraUsers = async (userRepository, bodyWork) => {
    for(const removeUsers of bodyWork) {
      userRepository.deleteUserByUserName(removeUsers.userName);
    }
}

const seekerUser = (bodyWork, workNode) => {
  return bodyWork.findIndex((currentUser) => {
    return currentUser.userName === workNode.userName;
  });
}

const adapterSourceUserFormat = (nodeUser) => {
  return {
    userName: nodeUser.login || undefined,
    externalId: nodeUser.node_id || undefined,
    externalSource: ((nodeUser.html_url && nodeUser.html_url.indexOf('//') != -1) ? nodeUser.html_url.split('//')[1].split('.')[0] : undefined),
    picture: nodeUser.avatar_url || undefined,
    email: nodeUser.email || undefined
  };
}
