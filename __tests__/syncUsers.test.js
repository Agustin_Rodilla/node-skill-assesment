const InMemoryUserRepository = require('../infrastructure/in-memory-user.repository')

describe('SyncUsers test', () => {
  const userRepository = new InMemoryUserRepository()

  const testUser = {
    login: 'Payflow',
    node_id: 'test1234',
    html_url: 'http://payflow.com',
    email: 'test@payflow.es'
  }


  const testUser2 = {
    login: 'Payflow2',
    node_id: 'test1234',
    html_url: 'http://payflow.com',
    email: 'test@payflow.es'
  }

  const testUserReload = {
    login: 'Payflow',
    node_id: 'testFFFFFF4',
    html_url: 'http://payflow.com',
    email: 'test@payflow.es'
  }

  const invalidUser = {
    node_id: 'test1234',
    email: 'test@payflow.es'
  }

  const invalidUser2 = {
    login: 'Payflowffff',
    node_id: 'test1234',
    html_url: 'http://forchan.com',
    email: 'test@payflow.es'
  }

  let saveIndo = undefined

  const mockRez = {
    sendStatus: function(codeResp){
      saveIndo = codeResp;
    }
  }

  const syncUsers = require('../routes/syncUsers');

  it('test Dummy errors in format', async () => {
    objUsers = {
                items: [testUser, invalidUser, invalidUser2]
            };
    const responseProcess = await syncUsers.managerResponseGitUsers(userRepository, objUsers);
    // console.error('responseProcess: ', responseProcess);
    expect(responseProcess[0]['error']).toBe('ValidationError: "userName" is required');
    expect(responseProcess[1]['error']).toBe('ValidationError: "externalSource" must be one of [bitbucket, github, payflow]');


    const responseProcess2 = await syncUsers.managerResponseGitUsers(userRepository, testUser);
    // console.error('responseProcess2: ', responseProcess2);
    expect(responseProcess2[0]['error']).toBe('invalitFormat, have no items');
  });

  it('test Dummy updates', async () => {
    objUsers = {
                items: [testUser, testUser2]
            };
    const responseProcess = await syncUsers.managerResponseGitUsers(userRepository, objUsers);
    expect(responseProcess.length).toBe(0);

    objUsers2 = {
                items: [testUserReload, testUser2]
            };
    const responseProcess2 = await syncUsers.managerResponseGitUsers(userRepository, objUsers2);
    expect(responseProcess2.length).toBe(0);
    reloadedUser = userRepository.findByUserName(testUserReload.login);
    expect(reloadedUser.externalId).toBe('testFFFFFF4');
  });

  it('test Dummy Clean', async () => {
    objUsers = {
                items: [testUser, testUser2]
            };
    const responseProcess = await syncUsers.managerResponseGitUsers(userRepository, objUsers);
    expect(responseProcess.length).toBe(0);

    removedUserZero = userRepository.findByUserName(testUser2.login);
    expect(removedUserZero.userName).toBe('Payflow2');

    objUsers2 = {
                items: [testUserReload]
            };
    const responseProcess2 = await syncUsers.managerResponseGitUsers(userRepository, objUsers2);
    expect(responseProcess2.length).toBe(0);
    reloadedUser = userRepository.findByUserName(testUserReload.login);
    expect(reloadedUser.externalId).toBe('testFFFFFF4');

    removedUser = userRepository.findByUserName(testUser2.login);
    expect(removedUser).toBe(undefined);
  });
})
